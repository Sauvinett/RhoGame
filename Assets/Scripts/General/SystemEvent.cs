﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SystemEvent : MonoBehaviour {

    public GameObject audioSource;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            //DontDestroyOnLoad(audioSource.gameObject);
        }
    }
}
