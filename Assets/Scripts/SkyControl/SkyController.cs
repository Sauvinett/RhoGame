﻿using UnityEngine;

public class SkyController : MonoBehaviour {

    public GameObject stars;
    public GameObject sunlight;
    public ParticleSystem starsParticles;

    private float speedRotation = 0.5f;
    private float angle;
    private float eulerAngleY;
    private float eulerAngleX;
    private float previousAngle;

    void Start()
    {
        starsParticles.Stop();
        previousAngle = angle;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.T))
        {
            speedRotation += 1.0f;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            speedRotation -= 1.0f;
        }

        eulerAngleX = sunlight.transform.eulerAngles.x;
        eulerAngleY = sunlight.transform.eulerAngles.y;

        if (eulerAngleY == 0)
        {
            angle = eulerAngleX;
        }
       else //eulerAngleY = 180
        {
            if (eulerAngleX >= 0 && eulerAngleX < 90f)
            {
                angle = 90f - 90f* ((eulerAngleX - 90f) / 90f);
            }
            if (eulerAngleX >= 270f && eulerAngleX < 360f)
            {
                angle = 180f - 90f * ((eulerAngleX - 360f) / 90f);
            }
        }

        angle = Mathf.Floor(angle);

        if (angle == 170)
        {
            starsParticles.Play();
            //Double check the rotation direction
            /*if (previousAngle < angle) starsParticles.Play();
            else starsParticles.Stop();*/
        }
        if(angle == 330)
        {
            starsParticles.Stop();
            //Double check the rotation direction
            /* if (previousAngle < angle) starsParticles.Stop();
             else starsParticles.Play();*/
        }

        sunlight.transform.Rotate(Time.deltaTime * speedRotation, 0, 0);
        stars.transform.rotation = sunlight.transform.rotation;
    }
}
