﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Animator playerAnimator;
    private CapsuleCollider playerCollider;

    // Use this for initialization
    void Start () {
        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        playerAnimator.SetFloat("Forward", Input.GetAxis("Vertical"));
        playerAnimator.SetFloat("Sided", Input.GetAxis("Horizontal"));
    }
}
